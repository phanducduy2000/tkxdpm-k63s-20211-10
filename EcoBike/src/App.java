import java.io.IOException;
import java.sql.SQLException;

import controller.LoginController;
import javafx.animation.FadeTransition;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import utils.Configs;
import views.screen.login.LoginScreen;

public class App extends Application {

	@FXML
	ImageView logo;


	public App() throws SQLException {
	}

	@Override
	public void start(Stage primaryStage) {
		try {

			// initialize the scene
			StackPane root = (StackPane) FXMLLoader.load(getClass().getResource(Configs.SPLASH_SCREEN_PATH));
			Scene scene = new Scene(root);
			primaryStage.setScene(scene);
			primaryStage.show();

			// Load splash screen with fade in effect
			FadeTransition fadeIn = new FadeTransition(Duration.seconds(2), root);
			fadeIn.setFromValue(0);
			fadeIn.setToValue(1);
			fadeIn.setCycleCount(1);

			// Finish splash with fade out effect
			FadeTransition fadeOut = new FadeTransition(Duration.seconds(1), root);
			fadeOut.setFromValue(1);
			fadeOut.setToValue(0);
			fadeOut.setCycleCount(1);

			// After fade in, start fade out
			fadeIn.play();
			fadeIn.setOnFinished((e) -> {
				fadeOut.play();
			});

			// After fade out, load actual content
			fadeOut.setOnFinished((e) -> {
				try {
//					HomeScreenHandler homeHandler = new HomeScreenHandler(primaryStage, Configs.HOME_PATH);
//					homeHandler.setScreenTitle("Home Screen");
//					homeHandler.setImage();
//
					//bike screen
//					BikeScreen bikeScreen= new BikeScreen(primaryStage, Configs.BIKE_SCREEN_PATH);
//					RentControllerSystem rentControllerSystem = new RentControllerSystem();	//------------phaỉ có set controller ở đây vì bikescreen là màn hình đầu tiên, phải khởi tạo rentcontroller, nếu không thì những controller phía sau sẽ bị trống(null)
//					bikeScreen.setController(rentControllerSystem);
//					bikeScreen.setScreenTitle("Bike Screen");
//					rentControllerSystem.searchBike("1111", bikeScreen);


//					VehicleListScreen vehicleListScreen = new VehicleListScreen(primaryStage, Configs.VEHICLE_LIST_SCREEN_PATH);
//					vehicleListScreen.setStationId(1);
//					vehicleListScreen.showVehicleList();
//					vehicleListScreen.show();

					//login
					LoginScreen loginScreen= new LoginScreen(primaryStage, Configs.LOGIN);
					LoginController loginController = new LoginController();
					loginScreen.setController(loginController);

					loginScreen.show();
//					//bikeScreen.setImage();
//					bikeScreen.show();

					//deposit screen
//					DepositScreen depositScreen= new DepositScreen(primaryStage, Configs.DEPOSIT_SCREEN_PATH);
//					depositScreen.setScreenTitle("Deposit Screen");
//					depositScreen.show();
				} catch (IOException | SQLException e1) {
					e1.printStackTrace();
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	public static void main(String[] args) throws SQLException {
		launch(args);
	}
	
}
